import pandas as pd # Librería de tratamiento de datos
import pickle

# load the model from disk
class Model_knn():
   def __init__(self):
        self.model = None

   def load(self, path):
      self.model = pickle.load(open(path, 'rb'))
      return self

   def predict(self, data):
      data=pd.DataFrame([data])
      result = self.model.predict(data)
      return result[0]