import pandas as pd # Librería de tratamiento de datos
import pickle

# load the model from disk
class Model_time_series():
   def __init__(self):
        self.model = None

   def load(self, path):
      self.model = pickle.load(open(path, 'rb'))
      return self

   def predict(self, data):
      data = data['steps']
      result = self.model.get_forecast(data).predicted_mean
      return result