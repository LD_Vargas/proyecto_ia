
import rpy2.robjects as robjects
from rpy2.robjects import numpy2ri
from rpy2.robjects.packages import importr

r = robjects.r
numpy2ri.activate()
class Model_regression_linear_multiple(object):
    def __init__(self):
        self.model = None

    def load(self, path):
        model_rds_path = "{}.rds".format(path)
        self.model = r.readRDS(model_rds_path)
        return self

    def predict(self, X):
        if self.model is None:
            raise Exception("There is no Model")
        x = robjects.ListVector(X)
        resolve = r.predict(self.model, x, probability=True)
        return resolve[0]
