from static.functions.models.model_regression_linear_multiple import Model_regression_linear_multiple
from static.functions.models.model_knn import Model_knn
from static.functions.models.model_time_series import Model_time_series

class Model():
   def __init__(self,model_name,type):
         self.model_name = model_name
         self.model = self.set_model(type)
         
   def set_model(self,type):
      location = "static/assets/model/{}".format(self.model_name)
      if type == "Regresión_Lineal_Multiple":
         loaded_model = Model_regression_linear_multiple().load(location)
      elif type == "KNN":
         loaded_model = Model_knn().load(location)
      elif type == "Series_De_Tiempo":
         loaded_model = Model_time_series().load(location)
      return loaded_model

   def get_model(self):
      return self.model

   def predict_model(self, data):
      result = self.model.predict(data)
      return result



