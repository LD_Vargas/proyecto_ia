from static.functions.model import Model
from flask import Flask, jsonify, request, render_template
#image
import base64
from urllib.parse import unquote
import requests
#from PIL import Image
import os
import static.assets.config.FaceAPIConfig as cnfg

app = Flask(__name__)

@app.route ( '/' ) 
def home_page ():
    return render_template('index.html')


@app.route('/test', methods=['GET', 'POST'])
def testfn():
    # GET request
    if request.method == 'GET':
        message = {'greeting':'Hello from Flask!'}
        return jsonify(message)  # serialize and use JSON headers
    # POST request
    if request.method == 'POST':
        print(request.get_json())  # parse as JSON
        return 'Sucesss', 200

######## Data fetch ############
dic_data_models = {
        "1":{"model_name":"Accidente cerebrovascular","path":"modelo_accidente", "type":"KNN",
                'resolve':{ '0':'No tendrá un accidente cerebrovascular', '1':'Tendrá un accidente cerebrovascular'}},
        "2":{"model_name":"Predecir el precio de un vehiculo", "path":"model_predict_price_car", "type":"Regresión_Lineal_Multiple"},
        "3":{"model_name":"Clasificar el tipo de cirrosis de un paciente", "path":"model_cirrhosis", "type":"KNN",
               'resolve':{ '1.0':'Micronodular', '2.0':'Macronodular','3.0':'Mixta'}},
        "4":{"model_name":"Clasificar la calidad del vino", "path":"model_wine_quality", "type":"KNN", 
                'resolve':{ '1':'Vino de buena calidad', '2':'Vino de mala calidad'} },
        "5":{"model_name":"Predecir la masa corporal de una persona", "path":"model_body_mass", "type":"Regresión_Lineal_Multiple",
                'measures':'IMC'},
        "6":{"model_name":"Clasificar si un cliente se va a pasar de compañia celular", "path":"model_client_churn", "type":"KNN",
                'resolve':{ '0':'El cliente no se pasará de compañia de celular', '1':'El cliente se pasará de compañia de celular'}},
        "7":{"model_name":"Predicir el precio del aguacate", "path":"model_price_avocado", "type":"Series_De_Tiempo"},
        "8":{"model_name":"Predecir la cantidad de crímenes por día en Londres", "path":"model_crime_London", "type":"Series_De_Tiempo"}, 
        "9":{"model_name":"Predecir la cantidad de crímenes por día en Chicago", "path":"model_crime_Chicago", "type":"Series_De_Tiempo"}, 
        "10":{"model_name":"Predecir la cantidad de paciente recuperados del covid 19", "path":"model_covid_patients", "type":"Series_De_Tiempo"}, 
        "11":{"model_name":"Predecir el índice de precio de los clientes de Walmart", "path":"model_walmart_sales", "type":"Series_De_Tiempo"}, 
        "12":{"model_name":"Predecir la cantidad de inventario de la compañía", "path":"model_sales", "type":"Series_De_Tiempo"}, 
        "13":{"model_name":"Predecir el precio de las acciones del mercado SP 500stock", "path":"model_shares_of_stock", "type":"Series_De_Tiempo"}, 
        "14":{"model_name":"Predecir el precio el bitcoin", "path":"model_bitcoin", "type":"Series_De_Tiempo"}, 
        "15":{"model_name":"Predecir las ventas de la compañía Rossmann", "path":"model_rossmann_sails", "type":"Series_De_Tiempo"}, 
}
@app.route('/getdata/<model_number>', methods=['GET','POST'])
def data_get(model_number):
    if request.method == 'POST': # POST request
        #get data by data_models and create of var that must have the models
        model_data = dic_data_models[model_number]
        type = model_data["type"]
        values = request.get_json()["values"] 
        model_name = model_data["model_name"]
        model_path = model_data["path"]
        #create of model and predict with values data
        print("VALUES ",values)
        model = Model(model_path,type)
        model_predict = str(model.predict_model(values))
        print('model_predict ',model_predict)
        if 'resolve' in model_data and str(model_predict) in model_data['resolve']:
            model_predict = model_data['resolve'][model_predict]
        elif 'measures' in model_data:
            model_predict = model_predict +" "+model_data['measures']
        #create dictionary and resolve with data get by model
        resolve = {"model_name":model_name, "model_predict":str(model_predict)}
        return jsonify(resolve)
    
    else: # GET request  
        return 'Ok'

@app.route('/photo', methods=['GET', 'POST'])
def photo():
    # GET request
    if request.method == 'GET':
        message = {'greeting':'get photo'}
        return jsonify(message)  # serialize and use JSON headers

    # POST request
    if request.method == 'POST':
        photo = unquote(request.get_json()['photo'])
        photo = photo.replace("data:image/png;base64","")
        photo = base64.b64decode(photo)
        decodeit = open('scanner.jpg', 'wb')
        decodeit.write(photo)
        decodeit.close()
        resolve = recognize_image()
        return jsonify(resolve), 200

def recognize_image():
    image_path = os.path.join('./scanner.jpg')
    image_data = open(image_path, "rb")

    subscription_key, face_api_url = cnfg.config()

    headers = {
        'Content-Type': 'application/octet-stream',
        'Ocp-Apim-Subscription-Key': subscription_key
    }

    params = {
        'returnFaceId': 'true',
        'returnFaceAttributes': 'age,gender,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,exposure,noise'
    }

    response = requests.post(face_api_url, params=params, headers=headers, data=image_data)
    response.raise_for_status()
    face = response.json()
    features = face[0] 
    return get_faceAttributes(features)

def get_faceAttributes(data):
    faceAttributes = data["faceAttributes"]["emotion"]
    print(faceAttributes)
    faceAttributes_Json = {}
    for key in faceAttributes:
        if faceAttributes[key] != 0:
            faceAttributes_Json[key] = faceAttributes[key]
    return faceAttributes_Json

if __name__ == "__main__": 
    app.run(debug=True)
    